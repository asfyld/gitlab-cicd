This repository houses the enhanced and updated project for the EKPA eLearning Academy DevOps Engineering program. The project incorporates GITLAB CI/CD Pipelines, building, and deploying to an Ubuntu server.

This project is based on a forked demo app, originally from benc-uk/python-demoapp.

Web App URL: http://159.223.29.243:5000/ (Digital Ocean)

